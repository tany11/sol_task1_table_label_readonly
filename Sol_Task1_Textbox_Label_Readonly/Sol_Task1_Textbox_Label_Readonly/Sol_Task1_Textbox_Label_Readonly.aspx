﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Sol_Task1_Textbox_Label_Readonly.aspx.cs" Inherits="Sol_Task1_Textbox_Label_Readonly.Sol_Task1_Textbox_Label_Readonly" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

              <asp:ScriptManager ID="scriptManger" runat="server">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>

                    <asp:TextBox ID="FirstName" runat="server" ReadOnly="true"></asp:TextBox><br />
                     <asp:TextBox ID="MiddleName" runat="server" ReadOnly="true"></asp:TextBox><br />
                     <asp:TextBox ID="LastName" runat="server" ReadOnly="true"></asp:TextBox><br>
                    <asp:Button ID="btnEdit" runat="server" Text="Edit" OnClick="btnEdit_Click" />
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" PostBackUrl="~/Webpage1aspx.aspx"  />
                     </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </form>
</body>
</html>
