﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_Task1_Textbox_Label_Readonly
{
    public partial class Webpage1aspx : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Page.PreviousPage != null)
            {
                
               
                
                string strName1 =
                    ((TextBox)PreviousPage.FindControl("FirstName")).Text;
                string strName2 =
                    ((TextBox)PreviousPage.FindControl("MiddleName")).Text;
                string strName3 =
                    ((TextBox)PreviousPage.FindControl("LastName")).Text;
                
                lblName1.Text = strName1;
                lblName2.Text = strName2;
                lblName3.Text = strName3;
               
            }

        }
    }
}