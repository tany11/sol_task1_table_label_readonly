﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_Task1_Textbox_Label_Readonly
{
    public partial class Sol_Task1_Textbox_Label_Readonly : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FirstName.Text = "Tanmay";
            MiddleName.Text = "Umesh";
            LastName.Text = "Joshi";
        }




        protected void btnEdit_Click(object sender, EventArgs e)
        {
            FirstName.ReadOnly = false;
            MiddleName.ReadOnly = false;
            LastName.ReadOnly = false;
        }
    }
}